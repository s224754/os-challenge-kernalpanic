//
// Created by surlykke on 11/20/24.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include "cache.h"

#include <pthread.h>

// Cache made by Martin Surlykke

cache_storage* caches[CACHE_SIZE] = {0};

// Mutex for thread safety
pthread_mutex_t cache_mutex = PTHREAD_MUTEX_INITIALIZER;

// Function to initialize the cache using mmap
void init_cache() {
    memset(caches, 0, CACHE_SIZE * sizeof(cache_storage*));  // Initialize all pointers to NULL
}


// Function to clean up the cache
void cache_destroy() {
    for (unsigned int i = 0; i < CACHE_SIZE; i++) {
        cache_storage* entry = caches[i];
        while (entry != NULL) {
            cache_storage* next = entry->next; // Save the pointer to the next entry
            free(entry);                       // Free the current entry
            entry = next;                      // Move to the next entry
        }
        caches[i] = NULL; // Reset the bucket to NULL to avoid unused pointers
    }
    printf("Cache destroyed successfully.\n");
}

// Index cache entries
unsigned int index_cache(const uint8_t *hash) {
    unsigned int result = 0;
    for (unsigned int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
        result += (hash[i] << 7) + (hash[i] >> 31);
    }
    return result % CACHE_SIZE;
}

// Find an entry in the cache based on the given hash pointer
cache_storage* find_in_cache(const uint8_t *hash) {
    unsigned int index = index_cache(hash);
    cache_storage* entry = caches[index];
    while (entry != NULL) {
        if (memcmp(entry->hash, hash, SHA256_DIGEST_LENGTH) == 0) {
            return entry;
        }
        entry = entry->next;
    }
    return NULL;
}

// Add an entry to the cache
void add_to_cache(const uint8_t *hash, const uint64_t solution) {
    unsigned int index = index_cache(hash);
    cache_storage* new_entry = malloc(sizeof(cache_storage));
    if (new_entry == NULL) {
        perror("Memory allocation error");
        exit(EXIT_FAILURE);
    }
    memcpy(new_entry->hash, hash, SHA256_DIGEST_LENGTH);
    new_entry->solution = solution;
    new_entry->next = caches[index];  // Insert at the head of the chain
    caches[index] = new_entry;
}
