# os-challenge-KernalPanic
## Welcome to the project os-challenge-KernalPanic

The project is a server written in C. The server can accept client connections, process requests given and return responses through hashing. At the moment, the project uses sha256 hashing to brute-force solutions to the requests given. Request can be given a priority, processing those with the highest priority first. 

3 immediate improvements were found which could make the base hashing more efficient, namely saving previously done hashes, utilizing concurrent multitasking, and storing elements in an efficient manner. 
For the milestone, this has been done through the following methods
1. Caching 
2. Multithreading 
3. Heap storage

which will be explained further. 

The following dependencies are required to implement these improvements
A. pthread - used for Multithreading support 
B. openssl - used for SHA-256 hashing. 


1. Caching

Caching consists of tracking whether an equal copy of the current request we are working with has already been processed. If it has, the solution to the request should be retrievable and send out to the client immediately. If the request has not been processed before. The resulting solution should be saved for future retrieval. 
This is done by creating a memory system were each entry has to variables, namely the request and its corresponding solution. Mutex has to be controlled, locked when a thread accesses memory, and unlocked when this is done to ensure safe handling of memory. 

2. Multithreading 

Multithreading consists of dividing a problem into different tasks which can be solved independetly. Hashing is ideal for this as multiple requests can be handled at the same time. The system handles this by creating a request queue, it takes in multiple requests, it then delegates these requests to any thread which is not active. The program has a predefined number of threads allocated, which can be changed if testing finds this beneficial. This functionality also uses Mutex to control requests given to threads.  

3. Heap storage 

Incoming requests have to be stored. When picking which request to store, the requests with the highest priority should be processed first. This creates an inefficiency when using lists, as all elements in the list potentially have to have their priority checked before processing can begin. This has time complexity O(n). Instead of this approach, a max heap is used. The elements are listed in a tree, where the larger the priority, the closer to the root node a request is stored. As the highest priority will always be processed first, this creates an O(1) time complexity for extracting the top element,
but the max_heap structure may be violated, so heapify_down must be called, this takes at worst O(log n). Therefore the overall worst case complexity is O(log n).

### Overall solution:
In the main file of our solution, we create the server socket and and begin listening for incomming requests. Furthermore we initialize the heap storage and the worker threads.
We then have one thread for taking incomming requests, and adding them to the max heap. Then the worker threads gets the requests from the queue and processes them. It uses a bruteforce approach, after checking the cache if the solution is already there and when the hash is found, it gets added to cache if its not there and send back to the client. This approach then gets run again and again, untill there are no more incomming requests.


# Project structure
As only a few, but efficient, additions have been made to the project so far, there has not been a large focus on project structure. 
The project structure conists of several files, these files include: 
- CMakeLists.txt 

This could be seen as a misc file which has to be there
- Makefile 

This file is used to build the runconfiguration for the server. Enabling the command "make server" in the terminal which then enables the server to be run using ./server 
- max_heap 

This c file contains logic for the max heap.
- Readme.md 

This file to navigate the project 
- Server

At the current point, the c file "server" handles both the server, requests, caching and computation of the requests. One improvement would be moving caching to its own class which could be done quite easily, and which will be handled for the next hand-in. 


# Repository structure
Branch structure has also not had the highest priority so far. Due to the open-ness of the assignment, many different directions were possible. As such, each teammember has looked at the project and come up with their own ideas for which improvements could be made. As this was done independently, not much focus was put on branch names. 

The main work structure, however, has been that Marcus has mainly gotten the project up and running working to get the sha256 hashing up and running.
Marcus has also been the one to build the max_heap functionality.

Oliver s224754 has mainly worked on multithreading. 

Martin s224793 has mainly worked on caching. 

Marcus s224750 has mainly worked on data structures and server socket creation.

Throughout the different branches, the main branch has been used to merge all improvements. 
The development of multithreading can be seen in the "concurrent" branch. 

# Experiment structure
The experiments for this project, can be found the branches "studentName_experiment_branch". But the documentation for the experiements can be found below.


# Experiment - Oliver Badike Hansen s224754

## Objective
The primary goal of this experiment is to compare the performance of a multithreaded server implementation against a non-threaded server implementation.

## Experiment Setup
1. **Non-Threaded Server**:
    - Requests are handled sequentially by a single worker thread.
    - This version serves as the baseline for performance comparison.

2. **Multithreaded Server**:
    - Utilizes multiple worker threads (8 threads).
    - Each thread fetches tasks from a priority queue (max heap) and processes them independently.

3. **Testing Parameters**:
    - **Server IP**: `192.168.101.10`
    - **Port**: `8080`
    - **Client Parameters**:
        - Seed: `3435245`
        - Total Requests: `30`
        - Start: `0`
        - Difficulty: `30000000`
        - Repetition Probability: `20%`
        - Delay: `600000μs`
        - Lambda: `1.5`

4. **Execution Environment**:
    - Vagrant-based Linux VMs as per the project specifications.
    - Same client and server configuration for fair comparison.

---

## Results

### Non-Threaded Server
- **Reliability**: 100%
- **Score**: 17,937,848

### Multithreaded Server
- **Reliability**: 100%
- **Score**: 2,800,808

---

## Analysis

### Reliability
Both implementations achieved 100% reliability, correctly solving all reverse hashing requests.

### Performance
The multithreaded server significantly outperformed the non-threaded server, achieving a much lower latency score. This indicates that parallel processing allowed the server to handle concurrent requests more efficiently.

### Resource Utilization
The multithreaded server better utilized CPU resources by leveraging multiple threads, reducing the time taken to process incoming requests.

### Priority Handling
Both implementations adhered to the priority-based scheduling mechanism specified in the challenge, but multithreading allowed high-priority tasks to be processed faster without blocking low-priority tasks unnecessarily.

---

## Conclusion

The multithreaded implementation demonstrates a significant improvement in server performance without compromising reliability. This highlights the advantage of multithreading in handling concurrent tasks in operating systems and similar applications. Based on these findings, the multithreaded version is recommended for the final submission.

---

## Project Structure
The files from this test can be found in the gitlab branch: "oliver_experiment_branch"

- `thread.c`: Final multithreaded server implementation.
- `nonthreaded.c`: Baseline non-threaded server implementation. This version is used for comparison. 
- `Makefile`: Build script for compiling the server code.
- `README.md`: Documentation of experiments and project details.

---

## Contributors to this experiment
- Oliver Badike Hansen s224754


# Experiment - Marcus Lilleør Christoffersen s224750
## Objective
This experiement motivates on making the solution the fastest possible. After implementing the first data structure, we began wondering, if we could improve on the performance by changing the data structure.
Therefore, we conducted this experiment.

## Experiment Setup

### DoublyLinkedList
Data structure consisting of nodes, containing pointers to the previous and the next nodes, along with information about the request_packet.
The advantages of this data structure is, the constant extraction time, since it will always be the first node. And since the structure is complete, when setting the new first node, pointing to the last we have a much faster extracting than max_heap.
The disadvantages comes in the insertion, which in this case will be linear time, since at worst the entire list needs to be checked before inserting.


### max_heap
An array functioning like a tree structure, where the largest element is always at the top, and children to the left always smaller and children to the right always larger.
The advantages of this structure is the insertion time takes in worst case O(log n). this is because at least log(n) layers must be checked. Therefore O(log n) time.
Extracting the largest element takes constant time, but making sure the max_heap structure is still satisfied, we again get the log(n) layers, meaning extracting, at worst, takes logarithmic time.


## Testing Parameters:

- Server IP: 192.168.101.10
- Port: 8080

### Client Parameters:
- Seed: 3435245
- Total Requests: 30
- Start: 0
- Difficulty: 30000000
- Repetition Probability: 20%
- Delay: 600000μs
- Lambda: 1.5

## Execution Environment:

Vagrant-based Linux VMs as per the project specifications.
Same client and server configuration for fair comparison.

### Results
To make sure running the experiment wasn't a fluke, we ran the final.sh 3 times for each data structure and summed up the result:
### max_heap
The result of the experiment with the max-heap implementation, got a score of: 10.371.391

### doubly linked list
The result of the experiment with the doubly linked list implementation, got a score of:  10.291.667


## Analysis
At first glance it doesn't look like the doubly linked list implementation did anything major for the score.
In fact the difference was: 10.371.391 - 10.291.667 = 79.724. This equates to roughly a 0.7% increase in performance.
One potential explanation for this observation could be, that since we have a lot of threads actively extracting from the data structure,
the full potential of either structure is not fully utilised, since it rarely have more than 10 entries at a time.

## Conclusion
To conclude the experiment, the performance different from these 2 methods were so small, that in the end, it does not really matter which
data structure we go with. Therefore, have chosen to go with the max heap implementation, since it is more robust and the insertion time is slightly faster than with the doubly linked list.


### Project structure
The doubly linked list implementation can be named in the appropriately named files. The current server.c makes use of the max_heap.c implementation as can be seen.

# Dynamic threading - Martin Holme Surlykke s224793

This experiment is 1 of 2 created by Martin. Both experiments were developed in parallel due to their overlap.

Therefore, decisions made in this experiment are possibly results of observations made in the other experiment "Batched Requests"

## Justification

Dynamic threading was implemented
to address the fluctuating workload of the server,
making sure resources are utilized to their fullest extent.
A static thread pool size can lead to inefficiencies:

Underutilization: Too few threads result in requests piling up in the queue, leading to increased latency.
Overutilization: Too many threads can be active with nothing to do, leading to waste of valuable resources.

Dynamic threading is implemented to circumvent these issues, the thesis thus becomes:

Dynmaic threading will improve the system by generating equal or greater performance while minimizing unused resources.

## Implementation

To implement this, a function "adjust_thread_pool" has been implemented. It checks the size of the current queue of requests.
If the number of threads does not match the number of yet-to-be handled requests, it creates new threads to take care of them.

The function was originally planned to have functionality to remove threads as well, this was however not implemented due to 2 reasons:

    1. The logic needed to implement functionality for removing threads was more substanital in scope than first anticipated.
       It did not fit well with the implementation of the server in our main program. These problems could have been fixed but were
       chosen to be out of scope due to reason 2. 
    2. When running the program, it was noticed that only a few threads would activate, never going above 4. This showed that request
       where being fed too slowly for highly parallel computing to be efficient. Meaning other methods of parallization might be more
       profitable to focus on.


## Results
Results where tested on a PC with 12 physical threads.

Running "run-client-final", the results are:

Prio stats: 394 85 17 3 1 0 0 0 0 0 0 0 0 0 0 0

Results: 100.00 2907604

The same configuration in the main file gave the results:

Prio stats: 394 85 17 3 1 0 0 0 0 0 0 0 0 0 0 0

Results: 100.00 2646373

### Interpretation

From these results, we see that the dynamic threads are considerably slower, showing only 90% the performance of our main method.

The main function is, however, utilizing 8 cores as the standard while the dynamic cores never passed 5 active cores. Meaning more
"performance per core". This definitely shows a potential for improvement, which would be exciting to dvelve deeper into.


## Further improvements

As shown, the performance did go down a bit, but the main issue found was the lack of possibility for parallelization out of the box.
From this the second experiment was proposed, where each request would instead be split into "batches" which could be processed in parallel,
potentially showing a bigger room for improvement. 

# Batching Experiment for Request Processing - Martin Holme Surlykke s224793

This experiment is 1 of 2 created by Martin. Both experiments were developed in parallel due to their overlap.

Therefore, decisions made in this experiment are possibly results of observations made in the other experiment "Dynamic Threads"

## Justification

In the main program, hashes are processed individually, while this is great for stability and readability, it could be a potential for lost
performance, especially in the case of parallelized processing. Due to this, this experiment has been set up, where hashing requests are split
up into batches which can be processed in tandem.

The intended outcome is an increase in performance, more specifically we hope to see:

    Reduced computation time by processing smaller subranges concurrently.
    Improved utilization of worker threads by dividing large tasks into smaller chunks.

## Implementation

To implement this functionality, a new function "Process_sub_batch" is created. The function is called by the request_handler.
The function then takes the request, and splits it in 2 equal parts. After doing this, a new "child thread" is created.
The original thread as well as the child thread then each process half of the request. If no solution is found by a thread, it passes the value:
uint64_MAX to filter the right answer. The answer is then sent back to the request manager and processed further as in the main function.


## Results

The platform used for this experiment has 12 threads

The main platform was run with multi-threading utilizing 8 threads.

Unfortunately, this funtionality has not shown the wished performance improvements. Testing this versus the main thread shows a drastic decrease in
performance, going from a performance score of roughly 2,500,000 to 16,000,000. One obvious reason for this would be the setup. Running 8 threads
doubling would create 12 threads. Since the platform used for testing only has 12 threads this means the last 4 threads will be immitated by the system
instead of physical threads. Secondly, each time a request is processed, a new thread has to be created and then removed again after processing.
This is definitely a reason for lower performance. A request with a difficulty of 30.000.000 might not take that long to process.
This implementation would probably show more efficiency the higher the difficulty of the hashing set. 

