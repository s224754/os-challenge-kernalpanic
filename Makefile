# Compiler and flags
CC = gcc
CFLAGS = -Wall -Wextra

# Source files and object files
SRCS = server.c max_heap.c cache.c
OBJS = $(SRCS:.c=.o)

# Output executable
TARGET = server

# Link OpenSSL library
LIBS = -lssl -lcrypto -lpthread

# Rule for building the executable
$(TARGET): $(OBJS)
	$(CC) -o $(TARGET) $(OBJS) $(LIBS) -O3

# Rule for compiling .c files into .o files
%.o: %.c messages.h
	$(CC) $(CFLAGS) -c $<

# Clean rule to remove object files and the executable
clean:
	rm -f $(OBJS) $(TARGET)
