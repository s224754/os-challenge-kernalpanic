//
// Created by Marcus Christoffersen (s224750) on 27/09/2024.
//

#include "max_heap.h"
#include <stdio.h>


//allocating memory for the heap to be the size of the max_size times the size of a request packet
void heap_init(Heap *heap, int max_size) {
    heap->data = (request_packet *) malloc(max_size * sizeof(request_packet));  // Allocate memory for the array
    if (heap->data == NULL) {
        perror("Failed to allocate memory for heap");
        exit(EXIT_FAILURE);
    }
    heap->size = 0;
    heap->capacity = max_size;
}


//Inserts an element in to the heap at the bottom, then calls heapify_up to move the entry to the correct position
void heap_insert(Heap *heap, request_packet request) {
    if (heap->size == heap->capacity) {
        perror("Heap is full");
        return;
    }

    int index = heap->size;
    heap->data[index] = request;
    heap->size++;
    heapify_up(heap, index);
}

//Extracts the first element of the heap(since that will always be the one with the highest priority)
//Then it calls heapify down, to make sure the structure is still intact and following the max heap rules.
request_packet heap_extract_max(Heap *heap) {
    request_packet request;
    request = heap->data[0];

    heap -> data[0] = heap -> data[heap -> size - 1];
    heap -> size--;
    heapify_down(heap, 0);
    return request;
}

//Goes through the heap making sure the rules are applied, if not swapping the positions of the parent and current index.
void heapify_up(Heap *heap, int index) {
    while (index > 0) {
        int parent = (index - 1) / 2;
        if (heap->data[index].priority <= heap->data[parent].priority) {
            break;
        }
        request_packet parent_data = heap->data[parent];

        heap->data[parent] = heap->data[index];

        heap->data[index] = parent_data;
        index = parent;
    }
}

//This function also makes sure the rules stay intact, but is a bit more complex, since it needs to check more than just the parent.
//It needs to check the priority of both the left and right child, while making sure, that the left child isn't null.
void heapify_down(Heap *heap, int index) {
    while (index < heap -> size){
        int parent = index;
        int left_child = index * 2 + 1;
        int right_child = index * 2 + 2;

        if (left_child >= heap -> size){
            break;
        }

        uint8_t left_child_prio = heap -> data[left_child].priority;

        if (right_child >= heap -> size){
            if (left_child_prio > heap -> data[parent].priority){
                request_packet left_child_data = heap -> data[left_child];

                heap -> data[left_child] = heap -> data[parent];
                heap -> data[parent] = left_child_data;

                break;
            }
        }

        uint8_t right_child_prio = heap -> data[right_child].priority;
        if (heap -> data[parent].priority >= left_child_prio && heap -> data[parent].priority >= right_child_prio){
            break;
        }

        if (left_child_prio > right_child_prio) {
            request_packet left_child_data = heap -> data[left_child];

            heap -> data[left_child] = heap -> data[parent];
            heap -> data[parent] = left_child_data;

            index = left_child;
        }
        else {
            request_packet right_child_data = heap -> data[right_child];

            heap -> data[right_child] = heap -> data[parent];
            heap -> data[parent] = right_child_data;

            index = right_child;
        }
    }
}

//A simple function to determine to return if the heap is empty or not, for conditional checking in other functions
int heap_is_empty(Heap *heap) {
    return heap->size == 0;
}

//Frees up the memory used by the heap, and sets all values to NULL/0
void heap_destroy(Heap *heap) {
    free(heap->data);
    heap->data = NULL;
    heap->size = 0;
    heap->capacity = 0;
}