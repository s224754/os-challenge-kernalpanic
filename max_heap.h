//
// Created by Marcus Christoffersen (s224750) on 27/09/2024.
//
#ifndef OS_CHALLENGE_KERNALPANIC_MAX_HEAP_H
#define OS_CHALLENGE_KERNALPANIC_MAX_HEAP_H
#include <stdint.h>  // For uint8_t, uint64_t
#include <stdlib.h>  // For size_t

// Constants for SHA256
#define SHA256_DIGEST_LENGTH 32

// Define the request_packet structure
typedef struct {
    uint8_t hash[SHA256_DIGEST_LENGTH];  // 32-byte SHA256 hash
    uint64_t start;                      // Start of range
    uint64_t end;                        // End of range
    uint8_t priority;                    // Priority of the request
    int client_socket;
} request_packet;

// Define the Heap structure
typedef struct {
    request_packet *data;  // Array to store heap elements (request_packet)
    int size;              // Current number of elements in the heap
    int capacity;          // Maximum capacity of the heap
} Heap;

// Function declarations for heap operations

//Function declarations for the max heap.
void heap_init(Heap *heap, int max_size);

void heap_insert(Heap *heap, request_packet request);

request_packet heap_extract_max(Heap *heap);

void heapify_up(Heap *heap, int index);

void heapify_down(Heap *heap, int index);

int heap_is_empty(Heap *heap);

void heap_destroy(Heap *heap);

#endif