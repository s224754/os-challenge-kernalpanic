#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include "messages.h"
#include "max_heap.h"
#include "cache.h"

#define PORT 8080                       // Server port
#define MAX_REQUEST_SIZE 500             // Maximum number of requests in the heap
#define NUM_WORKER_THREADS 8             // Number of worker threads for handling requests

Heap request_max_heap;                   // Max heap to store incoming requests

pthread_mutex_t queue_mutex = PTHREAD_MUTEX_INITIALIZER;  // Mutex for protecting access to the heap
pthread_cond_t queue_not_empty = PTHREAD_COND_INITIALIZER; // Condition variable to signal when heap is not empty

// Function to add a request to the priority queue (heap)
//Made by Marcus Christoffersen
void add_request_to_queue(request_packet request) {
    pthread_mutex_lock(&queue_mutex);    // Lock the heap
    heap_insert(&request_max_heap, request);  // Insert the request into the heap based on its priority
    pthread_cond_signal(&queue_not_empty);    // Signal any waiting thread that a new request is available
    pthread_mutex_unlock(&queue_mutex);  // Unlock the heap
}

// Function to retrieve a request from the priority queue (heap)
// Made by Marcus Christoffersen
request_packet get_request_from_queue() {
    pthread_mutex_lock(&queue_mutex);    // Lock the heap
    while (heap_is_empty(&request_max_heap)) {    // If heap is empty, wait for a signal
        pthread_cond_wait(&queue_not_empty, &queue_mutex);
    }
    request_packet request = heap_extract_max(&request_max_heap);  // Extract the highest priority request
    pthread_mutex_unlock(&queue_mutex);  // Unlock the heap
    return request;
}

// Function to compute the SHA256 hash for a given input
// Made by Oliver Badike Hansen
void compute_sha256(const uint64_t input, uint8_t output[SHA256_DIGEST_LENGTH]) {
    uint8_t input_bytes[8];              // Convert input to byte array
    memcpy(input_bytes, &input, sizeof(input)); // Copy input into byte array
    SHA256(input_bytes, sizeof(input_bytes), output);  // Perform SHA256 hashing
}

// Function to compute the solution by brute-force searching for a hash match between start and end range
// Made by Oliver Badike Hansen
uint64_t compute_solution(const uint64_t start, const uint64_t end, uint8_t hash[SHA256_DIGEST_LENGTH]) {
    for (uint64_t i = start; i <= end; i++) {
        uint8_t hash_result[SHA256_DIGEST_LENGTH];
        compute_sha256(i, hash_result);  // Compute SHA256 hash for the current number
        if (memcmp(hash, hash_result, SHA256_DIGEST_LENGTH) == 0) {  // If hash matches the target hash, return the number
            return i;
        }
    }
    return 0;
}
int counter = 1; // Global counter to track incoming requests

// Function to handle incoming client requests, parse the packet, and add it to the queue
// Made by Oliver Badike Hansen
void handle_request(const int client_socket) {
    uint8_t packet_size[PACKET_REQUEST_SIZE];   // Buffer to store the incoming request packet
    uint8_t hash[SHA256_DIGEST_LENGTH];         // Buffer to store the hash from the packet

    read(client_socket, packet_size, PACKET_REQUEST_SIZE);   // Read the request packet from the client
    memcpy(hash, packet_size + PACKET_REQUEST_HASH_OFFSET, SHA256_DIGEST_LENGTH);  // Extract hash from the packet
    const uint64_t start = be64toh(*(uint64_t *)(packet_size + PACKET_REQUEST_START_OFFSET)); // Extract start range
    const uint64_t end = be64toh(*(uint64_t *)(packet_size + PACKET_REQUEST_END_OFFSET));     // Extract end range
    uint8_t priority = packet_size[PACKET_REQUEST_PRIO_OFFSET]; // Extract request priority

    // Create a request packet object to add to the queue
    request_packet request;
    memcpy(request.hash, hash, SHA256_DIGEST_LENGTH);
    request.start = start;
    request.end = end;
    request.priority = priority;
    request.client_socket = client_socket; // Store the client socket for future response

    // Print information about the received request
    printf("Received request nr: %i, with priority: = %d\n", counter, priority);
    add_request_to_queue(request);  // Add the request to the priority queue (heap)
    counter++;
}


// Worker thread function to process requests from the queue
// Made by Oliver Badike Hansen
_Noreturn void *request_handler() {
    int count = 1;  // Local counter for the number of processed requests
    while (1) {
        request_packet request = get_request_from_queue();    // Fetch a request from the queue

        pthread_mutex_lock(&cache_mutex);  // Lock the cache mutex for safety
        cache_storage *cached_entry = find_in_cache(request.hash); // Check if already in cache
        pthread_mutex_unlock(&cache_mutex); // Unlock cache to let other threads access
        uint64_t result;

        if (cached_entry != NULL) {
            result = cached_entry->solution;
            printf("Solution found in cache for request nr %i: %lu\n", count, result);
        } else {
            // No cache entry found, compute the solution
            result = compute_solution(request.start, request.end, request.hash);
            add_to_cache(request.hash, result);  // Cache the computed solution
            printf("Solution computed and cached for request nr %i: %lu\n", count, result);
        }

        pthread_mutex_unlock(&cache_mutex);  // Unlock the cache mutex

        uint64_t network_result = htobe64(result);  // Convert to network byte order
        write(request.client_socket, &network_result, PACKET_RESPONSE_SIZE);  // Send the result back to the client
        printf("Solution sent for request nr %i: %lu\n", count, result);
        count++;
        close(request.client_socket);  // Close the client socket after sending the response
    }
}

// Made by Marcus Christoffersen
int main(int argc, char *argv[]) {
    heap_init(&request_max_heap, MAX_REQUEST_SIZE);   // Initialize the max heap for storing requests
    int server_socket;

    struct sockaddr_in server_address;   // Server address structure
    struct sockaddr_in client_address;   // Client address structure
    socklen_t client_address_length = sizeof(client_address);

    int domain = AF_INET;       // IPv4 protocol
    int socket_type = SOCK_STREAM;  // TCP socket
    int protocol = 0;          // System-chosen protocol, default is TCP

    // Default port number
    int port = PORT;  // Default port (8080)

    // If a port number is provided as a command-line argument, use it
    if (argc > 1) {
        port = atoi(argv[1]);  // Convert the port from string to integer
        if (port <= 0) {
            fprintf(stderr, "Invalid port number: %s\n", argv[1]);
            heap_destroy(&request_max_heap);
            exit(EXIT_FAILURE);
        }
    }
    // Create a socket for the server
    if ((server_socket = socket(domain, socket_type, protocol)) == -1) {
        perror("Failed to create socket");
        heap_destroy(&request_max_heap);
        exit(EXIT_FAILURE);
    }

    // Set up the server address
    server_address.sin_family = domain;
    server_address.sin_addr.s_addr = INADDR_ANY;  // Bind to any network interface
    server_address.sin_port = htons(port);        // Use the provided port number

    const struct sockaddr *bind_address = (struct sockaddr *)&server_address;
    const socklen_t bind_address_length = sizeof(server_address);
    // Bind the socket to the specified IP and port
    if (bind(server_socket, bind_address, bind_address_length) < 0) {
        perror("Bind failed");
        heap_destroy(&request_max_heap);
        close(server_socket);
        exit(EXIT_FAILURE);
    }

    // Listen for incoming connections
    const int n = 10;  // Number of pending connections queue, if more than 10 pending, it will fail.
    if (listen(server_socket, n) < 0) {
        perror("Listen failed");
        heap_destroy(&request_max_heap);
        close(server_socket);
        exit(EXIT_FAILURE);
    }
    printf("Server listening on port %d...\n", port);

    // Create a pool of worker threads for handling client requests
    // This part is made by Oliver Badike Hansen
    pthread_t worker_threads[NUM_WORKER_THREADS];
    for (int i = 0; i < NUM_WORKER_THREADS; i++) {
        pthread_create(&worker_threads[i], NULL, request_handler, NULL);  // Start each worker thread
    }

    // Main loop to accept and handle client connections
    while (1) {
        // Accept a new client connection
        const int client_socket = accept(server_socket, (struct sockaddr *)&client_address, &client_address_length);
        if (client_socket < 0) {
            perror("Accept failed");
            continue;
        }

        if (client_socket) {
            handle_request(client_socket);  // Handle the request from the client
        }

        // Check if the server socket is broken, and shut down if necessary
        if (server_socket < 0) {
            heap_destroy(&request_max_heap);   // Destroy the heap to free memory
            heap_destroy(&request_max_heap);
            close(server_socket);  // Close the server socket
            exit(EXIT_FAILURE);
        }
    }
}