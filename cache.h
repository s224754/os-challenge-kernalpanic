//
// Created by surlykke on 11/20/24.
//

#ifndef CACHE_H
#define CACHE_H

#include <stdint.h>
#include <pthread.h>

#define CACHE_SIZE 1024
#define SHA256_DIGEST_LENGTH 32

typedef struct cache_storage {
    uint8_t hash[SHA256_DIGEST_LENGTH];
    uint64_t solution;
    struct cache_storage *next;
} cache_storage;

extern cache_storage* caches[CACHE_SIZE];

extern pthread_mutex_t cache_mutex;

// Function prototypes
unsigned int index_cache(const uint8_t *hash);
cache_storage* find_in_cache(const uint8_t *hash);
void add_to_cache(const uint8_t *hash, uint64_t solution);
void init_cache();
void cache_destroy();

#endif // CACHE_H